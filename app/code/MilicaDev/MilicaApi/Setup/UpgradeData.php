<?php

namespace MilicaDev\MilicaApi\Setup;

use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

class UpgradeData implements UpgradeDataInterface
{
    protected $nameFactory;
    protected $infoFactory;
    protected $data;

    public function __construct(
        \MilicaDev\MilicaApi\Model\NameFactory $nameFactory,
        \MilicaDev\MilicaApi\Model\InfoFactory $infoFactory,
        \MilicaDev\MilicaApi\Helper\Data $data
    )
    {
        $this->nameFactory = $nameFactory;
        $this->infoFactory = $infoFactory;
        $this->data = $data;
    }

    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {

       
        
        $url = "https://www.systembolaget.se/api/assortment/stores/xml"; 
        $result = $this->data->ApiData($url);
        $oXML = new \SimpleXMLElement($result);

     

        foreach($oXML->ButikOmbud as $key){

            $setup->startSetup();


                $salesName = $this->nameFactory->create();
                $salesName->setName($key->Nr);
                $salesName->save();


                $bingo = $key->Address1." ".$key->Address3." ".$key->Address4;

                $info = $this->infoFactory->create();
                $info->setNameId($salesName->getId());
                $info->setAdress($bingo);
                $info->setTelefonNummer($key->Telefon);
                $info->setOppettider($key->Oppettider);
                $info->setNamn($key->Namn);
                $info->setTyp($key->Typ);
                $info->save();

         

            $setup->endSetup();
        }
    }
}
