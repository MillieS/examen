<?php

namespace MilicaDev\MilicaApi\Setup;

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

class InstallData implements InstallDataInterface
{
    private $infoSetupFactory;

    public function __construct(
        \MilicaDev\MilicaApi\Setup\InfoSetupFactory $infoSetupFactory
    )
    {
        $this->infoSetupFactory = $infoSetupFactory;
    }

    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        $infoEntity = \MilicaDev\MilicaApi\Model\Info::ENTITY;

        $infoSetup = $this->infoSetupFactory->create(['setup' => $setup]);

        $infoSetup->installEntities();



        $infoSetup->addAttribute(
            $infoEntity, 'typ', ['type' => 'varchar']
        );

     

        $setup->endSetup();
    }
}
