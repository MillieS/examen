<?php

namespace MilicaDev\MilicaApi\Setup;

use Magento\Eav\Setup\EavSetup;

class InfoSetup extends EavSetup
{
    public function getDefaultEntities()
    {
        $infoEntity = \MilicaDev\MilicaApi\Model\Info::ENTITY;
        $entities = [
            $infoEntity => [
                'entity_model' => 'MilicaDev\MilicaApi\Model\ResourceModel\Info',
                'table' => $infoEntity . '_entity',
                'attributes' => [
                    'name_id' => [
                        'type' => 'static',
                    ],
                    'adress' => [
                        'type' => 'static',
                    ],

                    'telefon_nummer' => [
                        'type' => 'static',
                    ],
                    'oppettider' => [
                        'type' => 'static',
                    ],

                     'namn' => [
                        'type' => 'static',
                    ],
                ],
            ],
        ];
        return $entities;
    }
}
