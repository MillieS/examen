<?php

namespace MilicaDev\MilicaApi\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        $infoEntityTable = \MilicaDev\MilicaApi\Model\Info::ENTITY . '_entity';
        $nameEntityTable = 'milicadev_milicaapi_name';

        $setup->getConnection()
            ->addForeignKey(
                $setup->getFkName($infoEntityTable, 'name_id', $nameEntityTable, 'entity_id'),
                $setup->getTable($infoEntityTable),
                'name_id',
                $setup->getTable($nameEntityTable),
                'entity_id',
                \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
            );

        $setup->endSetup();
    }
}
