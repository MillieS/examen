<?php

namespace MilicaDev\MilicaApi\Model;

class Info extends \Magento\Framework\Model\AbstractModel
{
    const ENTITY = 'milicadev_milicaapi_info';

    /**
     * Initialize info model
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init('MilicaDev\MilicaApi\Model\ResourceModel\Info');
    }
}
