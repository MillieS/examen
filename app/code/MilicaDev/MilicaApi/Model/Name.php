<?php

namespace MilicaDev\MilicaApi\Model;

class Name extends \Magento\Framework\Model\AbstractModel
{
    protected function _construct()
    {
        $this->_init('MilicaDev\MilicaApi\Model\ResourceModel\Name');
    }
}