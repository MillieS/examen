<?php

namespace MilicaDev\MilicaApi\Model\ResourceModel;

class Info extends \Magento\Eav\Model\Entity\AbstractEntity
{

    protected function _construct()
    {
        $this->_read = 'milicadev_milicaapi_info_read';
        $this->_write = 'milicadev_milicaapi_info_write';
       
    }

    /**
     * Getter and lazy loader for _type
     *
     * @throws \Magento\Framework\Exception\LocalizedException
     * @return \Magento\Eav\Model\Entity\Type
     */
    public function getEntityType()
    {
        if (empty($this->_type)) {
            
            $this->setType(\MilicaDev\MilicaApi\Model\Info::ENTITY); 
        }
        
        return parent::getEntityType();
    }

}