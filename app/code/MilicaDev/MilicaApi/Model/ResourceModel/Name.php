<?php

namespace MilicaDev\MilicaApi\Model\ResourceModel;

class Name extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected function _construct()
    {
        $this->_init('milicadev_milicaapi_name', 'entity_id');
    }
}