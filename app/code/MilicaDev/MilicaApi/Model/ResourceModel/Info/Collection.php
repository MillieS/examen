<?php

namespace MilicaDev\MilicaApi\Model\ResourceModel\Info;

class Collection extends \Magento\Eav\Model\Entity\Collection\AbstractCollection
    
{
    /**
     * Resource initialization
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('MilicaDev\MilicaApi\Model\Info', 'MilicaDev\MilicaApi\Model\ResourceModel\Info');
    }
}
