<?php

namespace MilicaDev\MilicaApi\Model\ResourceModel\Name;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected function _construct()
    {
        $this->_init(
            'MilicaDev\MilicaApi\Model\Name',
            'MilicaDev\MilicaApi\Model\ResourceModel\Name'
        );
    }
}
