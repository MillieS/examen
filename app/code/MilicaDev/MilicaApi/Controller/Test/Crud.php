<?php

namespace MilicaDev\MilicaApi\Controller\Test;

class Crud extends \MilicaDev\MilicaApi\Controller\Test
{
    protected $resultPageFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    )
    {
        $this->resultPageFactory = $resultPageFactory;
        return parent::__construct($context);
    }

   
    public function execute()
    {
        $resultPage = $this->resultPageFactory->create();
        $resultPage->getConfig()->getTitle()->prepend(__('Öppettider:'));

        return $resultPage;
    }
}