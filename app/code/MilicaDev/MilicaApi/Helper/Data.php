<?php

namespace MilicaDev\MilicaApi\Helper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    
 
    public function ApiData($url)
    {  
      
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_FAILONERROR,1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION,1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 15);
        $response = curl_exec($ch);          
        curl_close($ch);
    

        return $response;
    }
      
      
}