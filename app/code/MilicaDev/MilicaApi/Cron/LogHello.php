<?php

namespace MilicaDev\MilicaApi\Cron;

class LogHello
{
    protected $logger;
    protected $nameFactory;
    protected $infoFactory;
    protected $data;

    public function __construct(
        \Psr\Log\LoggerInterface $logger,
        \MilicaDev\MilicaApi\Model\NameFactory $nameFactory,
        \MilicaDev\MilicaApi\Model\InfoFactory $infoFactory,
        \MilicaDev\MilicaApi\Helper\Data $data
    
    )
    {
        $this->logger = $logger;   
        $this->nameFactory = $nameFactory;
        $this->infoFactory = $infoFactory;
        $this->data = $data;
    }
 

   
    public function execute()
    {
        $this->logger->info('Hello from Milicas job!');

        $url = "https://www.systembolaget.se/api/assortment/stores/xml"; 
        $result = $this->data->ApiData($url);
        $oXML = new \SimpleXMLElement($result);

     
        $i = 1;
        foreach($oXML->ButikOmbud as $key){

        $name = $this->nameFactory->create();
        $name->load($i);
        $name->setName($key->Nr);
        $name->save();
                                  
        $bingo = $key->Address1." ".$key->Address3." ".$key->Address4;
        $info = $this->infoFactory->create();
        $info->load($i);
        $info->setNameId($name->getId());
        $info->setAdress($bingo);
        $info->setTelefonNummer($key->Telefon);
        $info->setOppettider($key->Oppettider);
        $info->setNamn($key->Namn);
        $info->setTyp($key->Typ);
        $info->save();

         $i++;
                       
        
        }
        return $this;
    }
}


        
