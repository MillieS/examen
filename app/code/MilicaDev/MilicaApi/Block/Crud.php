<?php
namespace MilicaDev\MilicaApi\Block;

class Crud extends \Magento\Framework\View\Element\Template
{	 
	  protected $infoFactory;
  
	public function __construct(
		\Magento\Framework\View\Element\Template\Context $context, 
		\MilicaDev\MilicaApi\Model\InfoFactory $infoFactory)
	{
		    $this->infoFactory = $infoFactory;

        parent::__construct($context);
  }


    
        

	public function getOppettider()
  	{

          $collection =  $this->infoFactory->create()
                              ->getCollection();

          $collection->addAttributeToSelect('*');

          $collection->addAttributeToFilter('typ', array('like'=>'%Butik%'));

          return  $collection;
  	}
}